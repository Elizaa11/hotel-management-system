package com.petrova.hotels.repository;

import com.petrova.hotels.entity.ReservationExtra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationExtraRepository extends JpaRepository<ReservationExtra, Integer> {
}
