package com.petrova.hotels.controller;

import com.petrova.hotels.entity.Hotel;
import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.service.HotelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/hotels")
@Api(tags = "Hotels management")
@RequiredArgsConstructor
public class HotelController {

    private final HotelService service;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get all hotels", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get all")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the hotels successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(
                            code = 403,
                            message =
                                    "When currently logged user does not have correct roles to access this endpoint")
            })
    public PagedResult<Hotel> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(value = "Create hotel", notes = "Required roles: ADMIN, MANAGER", nickname = "create")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When hotel is created successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                                    "When currently logged user does not have correct roles to access this endpoint")
            })
    public ResponseEntity<Hotel> create(
            @Valid @RequestBody Hotel entity) {
        return new ResponseEntity<>(service.create(entity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get hotel by Id", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get by id")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the hotel successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When hotel doesn't exist.")
            })
    public ResponseEntity<Hotel> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(value = "Delete hotel by Id", notes = "Required roles: ADMIN, MANAGER", nickname = "delete")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When the hotel is deleted successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When hotel doesn't exists by Id")
            })
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Hotel with id of %s was deleted successfully!", id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @ApiOperation(value = "Edit hotel", notes = "Required roles: ADMIN, MANAGER", nickname = "replace")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When editing hotel successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When hotel doesn't exists by Id")
            })
    public ResponseEntity<Hotel> update(
            @PathVariable int id, @Valid @RequestBody Hotel entity) {

        service.getOne(id);
        entity.setId(id);
        return ResponseEntity.ok(service.update(entity));
    }

}