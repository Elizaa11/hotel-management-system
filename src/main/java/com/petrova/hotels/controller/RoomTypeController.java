package com.petrova.hotels.controller;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.RoomType;
import com.petrova.hotels.service.RoomTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/room-types")
@Api(tags = "Room types management")
@RequiredArgsConstructor
public class RoomTypeController {

    private final RoomTypeService service;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get all room types", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get all")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the room types successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public PagedResult<RoomType> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(value = "Create room type", notes = "Required roles: ADMIN, MANAGER", nickname = "create")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When room type is created successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public ResponseEntity<RoomType> create(
            @Valid @RequestBody RoomType entity) {
        return new ResponseEntity<RoomType> (service.create(entity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get room type by Id", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get by id")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the room type successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When room type doesn't exist.")
            })
    public ResponseEntity<RoomType> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(
            value = "Delete room type by Id",
            notes = "Required roles: ADMIN, MANAGER",
            nickname = "delete")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When the room type is deleted successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(
                            code = 403,
                            message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When room type doesn't exists by Id")
            })
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Room type with id of %s was deleted successfully!", id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @ApiOperation(value = "Edit room type", notes = "Required roles: ADMIN, MANAGER", nickname = "replace")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When editing room type successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When rooom type doesn't exists by Id")
            })
    public ResponseEntity<RoomType> update(
            @PathVariable int id, @Valid @RequestBody RoomType entity) {
        entity.setId(id);
        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }
}