package com.petrova.hotels.controller;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.Reservation;
import com.petrova.hotels.service.ReservationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/reservations")
@Api(tags = "Reservations management")
@RequiredArgsConstructor
public class ReservationController {

    private final ReservationService service;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get all reservations", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get all")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the reservations successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                                    "When currently logged user does not have correct roles to access this endpoint")
            })
    public PagedResult<Reservation> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Create reservation", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "create")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When reservation is created successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public ResponseEntity<Reservation> create(
            @Valid @RequestBody Reservation entity) {
        return new ResponseEntity<Reservation> (service.create(entity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get reservation by Id", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get by id")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the reservation successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When reservation doesn't exist.")
            })
    public ResponseEntity<Reservation> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(
            value = "Delete reservation by Id",
            notes = "Required roles: ADMIN, MANAGER, USER",
            nickname = "delete")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When the reservation is deleted successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(
                            code = 403,
                            message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When reservation doesn't exists by Id")
            })
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Reservation with id of %s was deleted successfully!", id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @ApiOperation(value = "Edit reservation", notes = "Required roles: ADMIN, MANAGER", nickname = "replace")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When editing reservation successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When reservation doesn't exists by Id")
            })
    public ResponseEntity<Reservation> update(
            @PathVariable int id, @Valid @RequestBody Reservation entity) {
        entity.setId(id);
        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }
}