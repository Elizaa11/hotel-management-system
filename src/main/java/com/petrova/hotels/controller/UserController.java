package com.petrova.hotels.controller;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.User;
import com.petrova.hotels.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
@Api(tags = "Employees management")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
public class UserController {

    private final UserService service;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(value = "Get all employees", notes = "Required roles: ADMIN, MANAGER", nickname = "get all")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the employees successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public PagedResult<User> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @ApiOperation(value = "Create employee", notes = "Required roles: ADMIN", nickname = "create")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When employee is created successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public ResponseEntity<User> create(
            @Valid @RequestBody User entity) {
        return new ResponseEntity<>(service.create(entity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(value = "Get employee by Id", notes = "Required roles: ADMIN, MANAGER", nickname = "get by id")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the employee successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When employee doesn't exist.")
            })
    public ResponseEntity<User> getOne(@PathVariable Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(
            value = "Delete user (employee) by Id",
            notes = "Required roles: ADMIN, MANAGER",
            nickname = "delete")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When the employee is deleted successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(
                            code = 403,
                            message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When employee doesn't exists by Id")
            })
    public ResponseEntity<?> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.ok(String.format("Employee with id of %s was deleted successfully!", id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @ApiOperation(value = "Edit employee", notes = "Required roles: ADMIN, MANAGER", nickname = "replace")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When editing employee successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When employee doesn't exists by Id")
            })
    public ResponseEntity<User> update(
            @PathVariable Long id, @Valid @RequestBody User entity) {
        entity.setId(id);
        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }
}
