package com.petrova.hotels.controller;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.ReservationExtra;
import com.petrova.hotels.service.ReservationExtraService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/reservation-extras")
@Api(tags = "Reservation extras management")
@RequiredArgsConstructor
public class ReservationExtraController {

    private final ReservationExtraService service;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get all reservation extras", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get all")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the reservation extras successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public PagedResult<ReservationExtra> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(value = "Create reservation extra", notes = "Required roles: ADMIN, MANAGER", nickname = "create")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When reservation extra is created successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public ResponseEntity<ReservationExtra> create(
            @Valid @RequestBody ReservationExtra entity) {
        return new ResponseEntity<>(service.create(entity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get reservation extra by Id", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get by id")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the reservation extra successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When reservation extra doesn't exist.")
            })
    public ResponseEntity<ReservationExtra> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(
            value = "Delete reservation extra by Id",
            notes = "Required roles: ADMIN, MANAGER",
            nickname = "delete")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When the reservation extra is deleted successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When reservation extra doesn't exists by Id")
            })
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Reservation extra with id of %s was deleted successfully!", id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @ApiOperation(value = "Edit reservation extra", notes = "Required roles: ADMIN, MANAGER", nickname = "replace")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When editing reservation extra successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When reservation extra doesn't exists by Id")
            })
    public ResponseEntity<ReservationExtra> update(
            @PathVariable int id, @Valid @RequestBody ReservationExtra entity) {
        entity.setId(id);
        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }
}