package com.petrova.hotels.controller;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.Room;
import com.petrova.hotels.service.RoomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/rooms")
@Api(tags = "Rooms management")
@RequiredArgsConstructor
public class RoomController {

    private final RoomService service;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get all rooms", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get all")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the rooms successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public PagedResult<Room> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(value = "Create room", notes = "Required roles: ADMIN, MANAGER", nickname = "create")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "When room is created successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint")
            })
    public ResponseEntity<Room> create(
            @Valid @RequestBody Room entity) {
        return new ResponseEntity<>(service.create(entity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_USER')")
    @ApiOperation(value = "Get room by Id", notes = "Required roles: ADMIN, MANAGER, USER", nickname = "get by id")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When get the room successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When room doesn't exist.")
            })
    public ResponseEntity<Room> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
    @ApiOperation(
            value = "Delete room by Id",
            notes = "Required roles: ADMIN, MANAGER",
            nickname = "delete")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When the room is deleted successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                                    "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When room doesn't exists by Id")
            })
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Room with id of %s was deleted successfully!", id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    @ApiOperation(value = "Edit room", notes = "Required roles: ADMIN, MANAGER", nickname = "replace")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "When editing room successfully"),
                    @ApiResponse(code = 401, message = "When user is not authenticated"),
                    @ApiResponse(code = 403, message =
                            "When currently logged user does not have correct roles to access this endpoint"),
                    @ApiResponse(code = 404, message = "When room doesn't exists by Id")
            })
    public ResponseEntity<Room> update(
            @PathVariable int id, @Valid @RequestBody Room entity) {
        entity.setId(id);
        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }
}