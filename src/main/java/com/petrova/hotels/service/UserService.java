package com.petrova.hotels.service;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.User;
import com.petrova.hotels.repository.RoleRepository;
import com.petrova.hotels.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository repository;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    private final RoleRepository roleRepository;

    public UserService(UserRepository repository, RoleRepository roleRepository) {
        this.repository = repository;
        this.roleRepository = roleRepository;
    }

    public PagedResult<User> getAll(Pageable pageable) {
        Page<User> entities = repository.findAll(pageable);
        return new PagedResult<User>(
                entities.getContent(),
                pageable.getPageNumber(),
                entities.getTotalPages(),
                entities.getTotalElements());
    }

    public User getOne(Long id) {
        Optional<User> employee = repository.findById(id);

        if (!employee.isPresent() || employee.get().isDeleted()) {
            throw new EntityNotFoundException("The user was not found.");
        }

        return employee.get();
    }

    public User create(User request) {
        request.setPassword(encoder.encode(request.getPassword()));
        request.getRoles().add(roleRepository.findByName("ROLE_USER").get());
        try{
            return repository.save(request);
        }
       catch(Exception ex){
            throw new EntityExistsException();
       }
    }

    @Transactional
    public void delete(Long id) {
        User account =
                this.repository
                        .findById(id)
                        .orElseThrow(() -> new EntityNotFoundException("User not found with id:" + id));

        account.setDeleted(true);
        this.repository.save(account);
    }

    public User update(User employee) {
        try {
            return repository.save(employee);
        }catch(Exception e){
            throw new IllegalArgumentException("Incorrect input data for user.");
        }
    }

    public Optional<User> findByUsername(String username){ return repository.findByEmail(username); }
    

}