package com.petrova.hotels.service;

import com.petrova.hotels.entity.Hotel;
import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.repository.HotelRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class HotelService {

    private HotelRepository repository;

    public HotelService(HotelRepository repository) {
        this.repository = repository;
    }

    public PagedResult<Hotel> getAll(Pageable pageable) {
        Page<Hotel> entities = repository.findAll(pageable);
        return new PagedResult<Hotel>(
                entities.getContent(),
                pageable.getPageNumber(),
                entities.getTotalPages(),
                entities.getTotalElements());
    }

    public Hotel getOne(int id) {
        Optional<Hotel> entity = repository.findById(id);

        if (!entity.isPresent()) {
            throw new EntityNotFoundException("The hotel was not found.");
        }
        return entity.get();
    }

    public Hotel create(Hotel entityRequest) {
        try{
            return repository.save(entityRequest);
        }
        catch(Exception ex){
            throw new EntityExistsException();
        }
    }

    public void deleteById(int id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(String.format("Hotel with id of %s was not found!", id));
        }
    }

    public Hotel update(Hotel entity) {
        try {
            return repository.save(entity);
        }catch(Exception e){
            throw new IllegalArgumentException("Incorrect input data for hotel.");
        }
    }
}