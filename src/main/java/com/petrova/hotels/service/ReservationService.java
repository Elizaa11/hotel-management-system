package com.petrova.hotels.service;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.Reservation;
import com.petrova.hotels.repository.ReservationRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class ReservationService {

    private ReservationRepository repository;

    public ReservationService(ReservationRepository repository) {
        this.repository = repository;
    }

    public PagedResult<Reservation> getAll(Pageable pageable) {
        Page<Reservation> entities = repository.findAll(pageable);
        return new PagedResult<Reservation>(
                entities.getContent(),
                pageable.getPageNumber(),
                entities.getTotalPages(),
                entities.getTotalElements());
    }

    public Reservation getOne(int id) {
        Optional<Reservation> entity = repository.findById(id);

        if (!entity.isPresent()) {
            throw new EntityNotFoundException("The reservation was not found.");
        }
        return entity.get();
    }

    public Reservation create(Reservation entityRequest) {
        try{
            return repository.save(entityRequest);
        }
        catch(Exception ex){
            throw new EntityExistsException();
        }
    }

    public void deleteById(int id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(String.format("Reservation with id of %s was not found!", id));
        }
    }

    public Reservation update(Reservation entity) {
        try {
            return repository.save(entity);
        }catch(Exception e){
            throw new IllegalArgumentException("Incorrect input data for reservation.");
        }
    }
}
