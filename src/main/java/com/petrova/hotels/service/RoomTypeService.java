package com.petrova.hotels.service;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.RoomType;
import com.petrova.hotels.repository.RoomTypeRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class RoomTypeService {

    private RoomTypeRepository repository;

    public RoomTypeService(RoomTypeRepository repository) {
        this.repository = repository;
    }

    public PagedResult<RoomType> getAll(Pageable pageable) {
        Page<RoomType> entities = repository.findAll(pageable);
        return new PagedResult<RoomType>(
                entities.getContent(),
                pageable.getPageNumber(),
                entities.getTotalPages(),
                entities.getTotalElements());
    }

    public RoomType getOne(int id) {
        Optional<RoomType> entity = repository.findById(id);

        if (!entity.isPresent()) {
            throw new EntityNotFoundException(String.format("Room type with id of %s was not found!", id));
        }
        return entity.get();
    }

    public RoomType create(RoomType entityRequest) {
        try{
            return repository.save(entityRequest);
        }
        catch(Exception ex){
            throw new EntityExistsException();
        }
    }

    public void deleteById(int id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(String.format("Room type with id of %s was not found!", id));
        }
    }

    public RoomType update(RoomType entity) {
        try {
            return repository.save(entity);
        }catch(Exception e){
            throw new IllegalArgumentException("Incorrect input data for room type.");
        }
    }
}
