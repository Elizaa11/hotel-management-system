package com.petrova.hotels.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "hotels")
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Size(min=2, max=100)
    private String name;

    @NotBlank
    @Size(min=2)
    private String address;

    @JsonIgnoreProperties("hotel")
    @OneToMany(mappedBy="hotel")
    private Set<User> users;

    @JsonIgnoreProperties("hotel")
    @OneToMany(mappedBy = "hotel")
    private Set<Room> rooms;
}