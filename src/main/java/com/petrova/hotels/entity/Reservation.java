package com.petrova.hotels.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@Table(name = "reservations")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonIgnoreProperties("reservations")
    @ManyToOne
    @JoinColumn(name="room_id", nullable=false)
    private Room room;

    @NotBlank
    private String client;

    private LocalDate startDate;

    private LocalDate endDate;

    private BigDecimal price;

    @JsonIgnoreProperties("reservations")
    @ManyToMany
    @JoinTable(
            name = "reservation_reservation_extra",
            joinColumns = @JoinColumn(name = "reservation_id"),
            inverseJoinColumns = @JoinColumn(name = "extra_id"))
    private Set<ReservationExtra> extras;
}