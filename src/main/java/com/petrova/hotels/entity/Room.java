package com.petrova.hotels.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Set;

@Data
@Entity
@Table(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    private int number;

    //price fot one night
    private BigDecimal price;

    @JsonIgnoreProperties("rooms")
    @ManyToOne
    @JoinColumn(name="type_id", nullable=false)
    private RoomType roomType;

    @JsonIgnoreProperties("rooms")
    @ManyToOne
    @JoinColumn(name="hotel_id", nullable=false)
    private Hotel hotel;

    @JsonIgnoreProperties("room")
    @OneToMany(mappedBy = "room")
    private Set<Reservation> reservations;
}